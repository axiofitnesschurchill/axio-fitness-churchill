We learn your exercise background, current fitness level, injuries/surgeries and goals you're looking to work toward and build a customized plan for you based on that. Visits to our studio are scheduled appointments and completely private. Nutrition counseling is available through a private meeting.

Address: 4531 Belmont Avenue, Youngstown, OH 44505, USA

Phone: 330-718-8009

Website: [https://axiofitness.com](https://axiofitness.com)
